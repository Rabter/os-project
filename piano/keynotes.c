#include <linux/module.h>
#include <linux/kernel.h>

#include "keynotes.h"
#include "sound.h"
#include "stack.h"

#define KEYS_IN_ROW 14

#define LEFT_1_OCTAVE 41
#define LEFT_2_OCTAVE 15
#define LEFT_3_OCTAVE 58
#define LEFT_4_OCTAVE 42

static const int notes[4][12] = {
{ 131, 139, 147, 156, 165, 175, 185, 196, 208, 220, 233, 247 },
{ 262, 277, 294, 311, 330, 349, 370, 392, 415, 440, 466, 494 },
{ 523, 554, 587, 622, 659, 698, 740, 784, 831, 880, 932, 988 },
{ 1047, 1109, 1175, 1245, 1319, 1397, 1480, 1568, 1661, 1760, 1865, 1976 }
};

static char is_played[4][12];

void clean_playing(void)
{
    int i = 0;
    while (i < OCTAVES_AMOUNT)
    {
        int j = 0;
        while (j < NOTES_IN_OCTAVE)
        {
            is_played[i][j] = 0;
            ++j;
        }
        ++i;
    }
}

void initialize_notes(void)
{
    printk(KERN_DEBUG "PIANO: initializing notes\n");
    // No notes are played by default
    clean_playing();
    sound_init();
    stack_init();
}

int check_key(int *octave, int *number, int *released, int key)
{
    if (key > 128)
    {
        key -= 128;
        *released = 1;
    }
    else
        *released = 0;

    if (key == LEFT_1_OCTAVE)
    {
        *octave = 0;
        *number = 0;
    }
    else if (key == LEFT_2_OCTAVE)
    {
        *octave = 1;
        *number = 0;
    }
    else if (key == LEFT_3_OCTAVE)
    {
        *octave = 2;
        *number = 0;
    }
    else if (key == LEFT_4_OCTAVE)
    {
        *octave = 3;
        *number = 0;
    }
    else
    {
        *octave = 0;
        while ((key < 2 || key > 12) && key > KEYS_IN_ROW && *octave < OCTAVES_AMOUNT)
        {
            ++*octave; //get 1 octave higher
            key -= KEYS_IN_ROW; // move 1 row lower
        }

        if (key < 2 || key > 12) // If the key is unsupported
            return 0;
        
        *number = key - 1;  
    }  
    return 1;
}

void key_pressed(int keynum)
{
    int octave, number, stop;
    printk(KERN_DEBUG "PIANO: analyzing key %d\n", keynum);
    if (check_key(&octave, &number, &stop, keynum))
    {
        printk(KERN_DEBUG "PIANO: key %d is on the piano\n", keynum);
        if (stop) // released
        {
            if (is_played[octave][number])
        	{
        	    printk(KERN_DEBUG "PIANO: key %d is active, stop=%d\n", keynum, stop);
        	    if (stack_peek() == notes[octave][number]) // Currencly playing note has just been released
        	    {
        	        int hz;
        	        stack_pop();
        	        hz = stack_peek();
        	        if (hz < 0)
        	        {
        	            sound_stop();
                        printk(KERN_DEBUG "PIANO: stopped sound\n");
                    }
        	        else
        	        {
                        sound_play(hz, 0); // play untill the key is released
                        printk(KERN_DEBUG "PIANO: restored %d %d\n", octave, number);
                    }
        	    }
        	    else
        	    {
        	        stack_erase(notes[octave][number]);
                    printk(KERN_DEBUG "PIANO: erased %d %d\n", octave, number);
        	    }
                is_played[octave][number] = 0; // The note is no longer being played
            }
        }
        else // pressed
        {
            if (!is_played[octave][number])
        	{
        	    int hz = notes[octave][number];
                sound_play(hz, 0); // play untill the key is released
                is_played[octave][number] = 1; // Mark as currently being played
                stack_push(hz);
        	    printk(KERN_DEBUG "PIANO: key %d is inactive, stop=%d\n", keynum, stop);
                printk(KERN_DEBUG "PIANO: started playing %d %d\n", octave, number);
            }
        }
    }
    else
        printk(KERN_DEBUG "PIANO: key %d is not on the piano\n", keynum);
}

void forcestop_notes(void)
{
    printk(KERN_DEBUG "PIANO: stopping all notes\n");
    clean_playing();
    stack_clean();
	sound_stop();
}

