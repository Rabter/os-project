#include <linux/module.h>
#include <linux/kernel.h>
#include "stack.h"

static int stack[STACK_MAXSIZE];
static int stack_size;

void stack_init(void)
{
    stack_size = 0;
}

int stack_pop(void)
{
    if (!stack_size)
    {
        printk(KERN_DEBUG "PIANO STACK: pop at empty stack\n");
        return -1;
    }
    printk(KERN_DEBUG "PIANO STACK: popped stack[%d]: %d\n", stack_size - 1, stack[stack_size - 1]);
    return stack[--stack_size];
}

int stack_peek(void)
{
    if (stack_size)
        return stack[stack_size - 1];
    else
        return -1;
}

void stack_push(int num)
{
    if (stack_size < STACK_MAXSIZE)
    {
        printk(KERN_DEBUG "PIANO STACK: stack[%d] is now %d\n", stack_size, num);
        stack[stack_size++] = num;
    }
}

void stack_erase(int num)
{
    int i = 0;
    char found = 0;
    while (i < stack_size && !found)
    {
        if (stack[i] == num)
            found = 1;
        ++i;
    }
    if (found)
    {
        printk(KERN_DEBUG "PIANO STACK: erasing stack[%d]: %d\n", i, stack[i]);
        while (i < stack_size)
        {
            stack[i - 1] = stack[i];
            ++i;
        }
        --stack_size;
    }
}

void stack_clean(void)
{
    stack_size = 0;
    printk(KERN_DEBUG "PIANO STACK: cleaning stack\n");
}
