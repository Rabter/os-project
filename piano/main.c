#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <asm/io.h>

#include "keynotes.h"

MODULE_LICENSE ("GPL");

static char stop_playing, space_pressed, ctrl_pressed;

// This function services keyboard interrupts
irq_handler_t keyboard_irq (int irq, void *dev_id, struct pt_regs *regs)
{
    unsigned char scancode;

    scancode = inb (0x60); // Read affected key code
    
    if (scancode == 29) // ctrl is pressed
    {
        ctrl_pressed = 1;
        if (space_pressed)
        {
            if (stop_playing)
            {
                stop_playing = 0;
                printk(KERN_INFO "PIANO: unmuted\n");
            }
            else
            {
                stop_playing = 1;
                forcestop_notes();
                printk(KERN_INFO "PIANO: muted\n");
            }
        }
    }
    else if (scancode == 57) // space is pressed
    {
        space_pressed = 1;
        if (ctrl_pressed)
        {
            if (stop_playing)
            {
                stop_playing = 0;
                printk(KERN_INFO "PIANO: unmuted\n");
            }
            else
            {
                stop_playing = 1;
                forcestop_notes();
                printk(KERN_INFO "PIANO: muted\n");
            }
        }
    }
    else if (scancode == 157)
        ctrl_pressed = 0;
    else if (scancode == 185)
        space_pressed = 0;
    else if (!stop_playing)
        key_pressed((int)scancode);

    return (irq_handler_t)IRQ_HANDLED;
}

// Initialize the module and Register the IRQ handler
static int __init piano_init(void)
{
    int result;
    
    printk(KERN_INFO "PIANO: initializing\n");
    
    // Request IRQ 1, the keyboard IRQ    
    result = request_irq (1, (irq_handler_t) keyboard_irq, IRQF_SHARED, "piano_key_analizer", (void *)(keyboard_irq));

    if (result)
    {
        printk(KERN_INFO "PIANO: can't get shared interrupt for keyboard\n");
        return result;
    }

    initialize_notes();
    
    ctrl_pressed = space_pressed = stop_playing = 0;

    printk(KERN_INFO "PIANO: ready to play\n");

    return 0;
}

// Remove the interrupt handler
static void __exit piano_exit(void)
{
    forcestop_notes();
    free_irq(1, (void *)(keyboard_irq)); // Must not pass NULL, this is a shared interrupt handler
    printk(KERN_INFO "PIANO: module removed\n");
}

module_init(piano_init);
module_exit(piano_exit);

