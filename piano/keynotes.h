#ifndef _KEYNOTES_H_
#define _KEYNOTES_H_

#define OCTAVES_AMOUNT 3
#define NOTES_IN_OCTAVE 12 // this considers flat notes aswell, like Db (Re flat)

void initialize_notes(void);
int check_key(int *octave, int *number, int *released, int key);
void key_pressed(int keynum);
void forcestop_notes(void);

#endif

