#ifndef _SOUND_H_
#define _SOUND_H_

void sound_init(void);
void sound_stop(void);
void sound_play(int frequency, int duration);

#endif

