#ifndef _STACK_H_
#define _STACK_H_

#define STACK_MAXSIZE 50

void stack_init(void);
int stack_pop(void);
int stack_peek(void);
void stack_push(int num);
void stack_erase(int num);
void stack_clean(void);

#endif

