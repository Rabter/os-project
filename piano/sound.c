#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/vt_kern.h>

#include "sound.h"

static int inf_played;

void sound_init()
{
    inf_played = -1; // A frequency that is to be played continuosly
}

void sound_stop(void)
{
    inf_played = -1;
    kd_mksound(0, 1);
}

void sound_play(int frequency, int duration)
{
    if ((!duration && (frequency == inf_played)) || !frequency)
        sound_stop();
    else
    {
        int jiffies = msecs_to_jiffies(duration);
        kd_mksound(frequency, jiffies);
        if (duration)
            inf_played = -1;
        else
            inf_played = frequency;
    }
}

