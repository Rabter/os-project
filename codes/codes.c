#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <asm/io.h>

MODULE_LICENSE ("GPL");

// This function services keyboard interrupts
irq_handler_t keyboard_irq (int irq, void *dev_id, struct pt_regs *regs)
{
    unsigned char scancode;

    scancode = inb (0x60); // Read affected key code
    printk(KERN_INFO "CODES: recieved key code %d\n", scancode);

    return (irq_handler_t)IRQ_HANDLED;
}

// Initialize the module and Register the IRQ handler
static int __init codes_init(void)
{
    int result;
    
    printk(KERN_INFO "CODES: initializing\n");
    
    // Request IRQ 1, the keyboard IRQ    
    result = request_irq (1, (irq_handler_t) keyboard_irq, IRQF_SHARED, "key_analizer", (void *)(keyboard_irq));

    if (result)
    {
        printk(KERN_INFO "CODES: can't get shared interrupt for keyboard\n");
        return result;
    }

    printk(KERN_INFO "CODES: ready to scan\n");

    return 0;
}

// Remove the interrupt handler
static void __exit codes_exit(void)
{
    free_irq(1, (void *)(keyboard_irq)); // Must not pass NULL, this is a shared interrupt handler
    printk(KERN_INFO "CODES: module removed\n");
}

module_init(codes_init);
module_exit(codes_exit);

